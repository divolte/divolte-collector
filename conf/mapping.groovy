mapping {
  map eventParameter('uuid') onto 'uuid'
  map eventParameter('policy_no') onto 'policy_no'
  map eventParameter('functionality_id') onto 'functionality_id'
  map eventParameter('use_case_id') onto 'use_case_id'
  map eventParameter('timestamp') onto 'timestamp'
  map eventParameter('source_system_id') onto 'source_system_id'
  map eventParameter('penny_test_failure') onto 'penny_test_failure'
  map eventParameter('address_change_status') onto 'address_change_status'
  map eventParameter('alternate_mobile_number_update_status') onto 'alternate_mobile_number_update_status'
  map eventParameter('autopay_status') onto 'autopay_status'
  map eventParameter('complaint_identifier') onto 'complaint_identifier'
  map eventParameter('customer_portal_website_trigger') onto 'customer_portal_website_trigger'
  map eventParameter('chatbot_trigger') onto 'chatbot_trigger'
  map eventParameter('deviation_in_tat') onto 'deviation_in_tat'
  map eventParameter('document_upload_failed') onto 'document_upload_failed'
  map eventParameter('eia_trigger') onto 'eia_trigger'
  map eventParameter('email_id_update_status') onto 'email_id_update_status'
  map eventParameter('fund_trigger') onto 'fund_trigger'
  map eventParameter('invalid_document_size') onto 'invalid_document_size'
  map eventParameter('invalid_document_type') onto 'invalid_document_type'
  map eventParameter('mobile_number_update_status') onto 'mobile_number_update_status'
  map eventParameter('multiple_clicks_counter') onto 'multiple_clicks_counter'
  map eventParameter('multiple_clicks_trigger') onto 'multiple_clicks_trigger'
  map eventParameter('multiple_times_sr_trigger') onto 'multiple_times_sr_trigger'
  map eventParameter('nominee_change_status') onto 'nominee_change_status'
  map eventParameter('nach_status') onto 'nach_status'
  map eventParameter('nps_status') onto 'nps_status'
  map eventParameter('not_setup_trigger') onto 'not_setup_trigger'
  map eventParameter('not_updated_in_last_year_trigger') onto 'not_updated_in_last_year_trigger'
  map eventParameter('neft_update_status') onto 'neft_update_status'
  map eventParameter('premium_due_alert_status') onto 'premium_due_alert_status'
  map eventParameter('preferred_communication_mode_trigger') onto 'preferred_communication_mode_trigger'
  map eventParameter('pending_document_flag') onto 'pending_document_flag'
  map eventParameter('premium_due_date') onto 'premium_due_date'
  map eventParameter('payment_method') onto 'payment_method'
  map eventParameter('payment_status') onto 'payment_status'
  map eventParameter('payment_success') onto 'payment_success'
  map eventParameter('payment_incomplete_trigger') onto 'payment_incomplete_trigger'
  map eventParameter('product_recommendation_next_login') onto 'product_recommendation_next_login'
  map eventParameter('premium_payment_status') onto 'premium_payment_status'
  map eventParameter('product_recommendation_trigger_NAV') onto 'product_recommendation_trigger_NAV'
  map eventParameter('product_recommendation_trigger') onto 'product_recommendation_trigger'
  map eventParameter('product_selected') onto 'product_selected'
  map eventParameter('payment_trigger') onto 'payment_trigger'
  map eventParameter('pan_update_status') onto 'pan_update_status'
  map eventParameter('pan_update_trigger') onto 'pan_update_trigger'
  map eventParameter('product_recommendation_whatsapp_email_sms') onto 'product_recommendation_whatsapp_email_sms'
  map eventParameter('setup_autopay_trigger') onto 'setup_autopay_trigger'
  map eventParameter('standing_instruction_trigger') onto 'standing_instruction_trigger'
  map eventParameter('si_setup_status') onto 'si_setup_status'
  map eventParameter('whatsapp_trigger') onto 'whatsapp_trigger'
  map eventParameter('redflag_trigger') onto 'redflag_trigger'
  map eventParameter('type_of_customer') onto 'type_of_customer'
  map eventParameter('transaction_incomplete_trigger') onto 'transaction_incomplete_trigger'
  map eventParameter('unclaimed_fund_trigger') onto 'unclaimed_fund_trigger'
  map eventParameter('validation_trigger') onto 'validation_trigger'
  map eventParameter('idle_flag') onto 'idle_flag'
  map eventParameter('resend_trigger') onto 'resend_trigger'
  map eventParameter('view_more_clicked') onto 'view_more_clicked'
  map eventParameter('page_no') onto 'page_no'
  map eventParameter('whatsapp_opt_in') onto 'whatsapp_opt_in'
  map eventParameter('support_opted') onto 'support_opted'
  map eventParameter('preferred_communication_language') onto 'preferred_communication_language'
  map eventParameter('payment_method_change') onto 'payment_method_change'
  map eventParameter('payment_frequency_change') onto 'payment_frequency_change'
  map eventParameter('link_my_family_member_policy') onto 'link_my_family_member_policy'
  map eventParameter('download_statement') onto 'download_statement'
  map eventParameter('nav_alert') onto 'nav_alert'
  map eventParameter('mobile_number') onto 'mobile_number'
  map eventParameter('banner_id') onto 'banner_id'
  map eventParameter('banner_location') onto 'banner_location'
  map eventParameter('click_on_submit') onto 'click_on_submit'
  map eventParameter('click_on_cancel') onto 'click_on_cancel'
  map eventParameter('email_id') onto 'email_id'
}